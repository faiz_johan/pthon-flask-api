TESTING THE APPLICATION

Run application by execute command "python3 main.py"

TEST BY USING POSTMAN

1. Display All Customers
    - Method: GET
    - http://127.0.0.1:5000/customers

    - Sample Response:
    - HTTP 200
    [
        {
            "email": "mohdfaizjohan@gmail.com",
            "id": 1,
            "name": "FaizJ",
            "pwd": "pbkdf2:sha256:260000$de08OZWvUA0pNEU4$49b4015855448e55cf92dabcf8bb7c36fd999962b2f1848b209fa05a6ff11bb1"
        },
        {
            "email": "faizjohan89@gmail.com",
            "id": 2,
            "name": "Mohd Faiz Md Johan",
            "pwd": "pbkdf2:sha256:260000$i8GA9P4HYdloIsQo$63bc75ea7ef0caa5e1690189f77f8097057e697a5a155fa0a8a5f7aac00055b0"
        }
    ]

2. Add New Customer
    - Method: POST
    - http://127.0.0.1:5000/add

    - Sample Request Body (JSON):
    {
        "name": "Mohd Faiz Md Johan",
        "email": "faizjohan89@gmail.com",
        "pwd":"abc123"
    }

    - Sample Response:
    - HTTP 200
    "Customer added successfully!"

3. Update Customer Detail
    - Method: PUT
    - http://127.0.0.1:5000/update

    - Sample Request Body (JSON):
    {
        "id": 1,
        "name":"Mohd Faiz",
        "email":"mohdfaizjohan@gmail.com",
        "pwd":"abc123"
    }

    - Sample Response:
    - HTTP 200
    "Customer updated successfully!"

4. Display Specific Customer Details
    - Method: GET
    - http://127.0.0.1:5000/customer/2

    - Sample Response:
    - HTTP 200
    {
        "email": "faizjohan89@gmail.com",
        "id": 2,
        "name": "Mohd Faiz Md Johan",
        "pwd": "pbkdf2:sha256:260000$i8GA9P4HYdloIsQo$63bc75ea7ef0caa5e1690189f77f8097057e697a5a155fa0a8a5f7aac00055b0"
    }

5. Delete a customer
    - Method: DELETE
    - http://localhost:5000/delete/2

    - Sample Response:
    - HTTP 200
    "Customer deleted successfully!"